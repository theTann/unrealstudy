// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Study01.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class STUDY01_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void PostInitializeComponents() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *hilt;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *pommel;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *blade;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *crossGuard;

	UPROPERTY(EditAnywhere, Category=Stat, Meta = (AllowPrivateAccess = true))
	float rotateSpd;
};
