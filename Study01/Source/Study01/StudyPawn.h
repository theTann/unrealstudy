// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Study01.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "StudyPawn.generated.h"

UCLASS()
class STUDY01_API AStudyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AStudyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;
	virtual void PossessedBy(AController* NewController) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere)
	UCapsuleComponent* capsule;

	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* mesh;

	UPROPERTY(VisibleAnywhere)
	UFloatingPawnMovement* movement;

	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* springArm;

	//UPROPERTY(VisibleAnywhere)
	//UCameraComponent* camera;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
private:
	void UpDown(float scale);
	void LeftRight(float scale);
};
