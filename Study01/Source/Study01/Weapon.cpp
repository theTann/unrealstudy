// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"

// Sets default values
AWeapon::AWeapon()
{
	UE_LOG(tann, Log, TEXT("AWeapon contructor start"));
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	hilt = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("hilt"));
	pommel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("pommel"));
	blade = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("blade"));
	crossGuard = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("crossGuard"));

	RootComponent = hilt;
	pommel->SetupAttachment(hilt);
	blade->SetupAttachment(hilt);
	crossGuard->SetupAttachment(hilt);

	ConstructorHelpers::FObjectFinder<UStaticMesh> crossGuardObj(TEXT("/Game/ModularWeapons/Meshes/Swords/Crossguards/Sword_crossguard_08"));
	
	if(crossGuardObj.Succeeded() == true)
	{
		UE_LOG(tann, Log, TEXT("Succeeded"));
		crossGuard->SetStaticMesh(crossGuardObj.Object);
	}
	else
	{
		UE_LOG(tann, Warning, TEXT("Failure"));
	}

	rotateSpd = 30.f;
	UE_LOG(tann, Log, TEXT("AWeapon contructor end"));
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0, rotateSpd * DeltaTime, 0));
}

void AWeapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	UE_LOG(tann, Log, TEXT("end play"));
}

void AWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(tann, Log, TEXT("weapon initialized."));
}
