// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Study01.h"
#include "GameFramework/Character.h"
#include "StudyCharacter.generated.h"

UCLASS()
class STUDY01_API AStudyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AStudyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* springArm;

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* camera;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool isAttacking;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, 
						AActor* OtherActor, 
						UPrimitiveComponent* OtherComp, 
						int32 OtherBodyIndex, 
						bool bFromSweep, 
						const FHitResult &SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, 
						AActor* OtherActor, 
						UPrimitiveComponent* OtherComp, 
						int32 OtherBodyIndex);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, 
				AActor* OtherActor, 
				UPrimitiveComponent* OtherComponent, 
				FVector NormalImpulse, 
				const FHitResult& Hit);

	virtual void PostInitializeComponents() override;
	
	UFUNCTION()
	void OnAttackMontageEnded(UAnimMontage* montage, bool bInterupted);
	

private:
	void UpDown(float scale);
	void LeftRight(float scale);

	void AttackAction();

	void AttackCheck();
	void AnimChange();
	void ReleaseSpace();
};
