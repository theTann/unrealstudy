// 공부해 볼것.
// 폰을 생성하지 않고 이미 있는 폰에 빙의할수있음.

#pragma once

#include "Study01.h"
#include "GameFramework/GameModeBase.h"
#include "StudyGameMode.generated.h"

// 에디터의 플레이버튼 -> 입장
// 입장하는 것을 엔진에서는 로그인이라 부름
// 로그인 완료시 PostLogin함수가 불림.
// 컨트롤러에 폰이 연결되는 과정을 빙의(possess)라고함.
// 플레이어컨트롤러생성 -> PostLogin순서임.
// PostLogin에서는 폰생성 -> 빙의 -> 게임시작

UCLASS()
class STUDY01_API AStudyGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AStudyGameMode();

public:
	virtual void PostLogin(APlayerController* NewPlayer) override;
};
