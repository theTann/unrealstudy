// Fill out your copyright notice in the Description page of Project Settings.

#include "StudyPawn.h"
#include "StudyAIController.h"

// Sets default values
AStudyPawn::AStudyPawn()
{
	UE_LOG(tann, Log, TEXT("AStudyPawn contructor start"));
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("capsule"));
	mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("skel_mesh"));
	movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("movement"));
	springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("springArm"));
	// camera = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));

	RootComponent = capsule;
	mesh->SetupAttachment(capsule);
	springArm->SetupAttachment(capsule);
	// camera->SetupAttachment(springArm);

	capsule->SetCapsuleHalfHeight(88.0f);
	capsule->SetCapsuleRadius(34.0f);
	mesh->SetRelativeLocationAndRotation(FVector(0, 0, -85), FRotator(0, -90, 0));
	springArm->TargetArmLength = 1000.0f;
	springArm->SetRelativeRotation(FRotator(-15.0f, 0, 0));

	ConstructorHelpers::FObjectFinder<USkeletalMesh> meshObj(TEXT("/Game/AdvancedLocomotionV3/Characters/Mannequin/Mannequin"));

	if (meshObj.Succeeded() == true) 
	{
		mesh->SetSkeletalMesh(meshObj.Object);
	}

	// by animation blueprint
	mesh->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> animBP(TEXT("/Game/AnimBluePrint/StudyAnimBP"));
	if (animBP.Succeeded() == true)
	{
		mesh->SetAnimInstanceClass(animBP.Class);
	}

	AIControllerClass = AStudyAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	UE_LOG(tann, Log, TEXT("AStudyPawn contructor end"));
}

// Called when the game starts or when spawned
void AStudyPawn::BeginPlay()
{
	Super::BeginPlay();

	// by code.
	//mesh->SetAnimationMode(EAnimationMode::AnimationSingleNode);
	//UAnimationAsset* AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("AnimSequence'/Game/AdvancedLocomotionV3/Characters/Mannequin/Animations/Locomotion/ALS_LF_Walk_F.ALS_LF_Walk_F'"));
	//if (AnimAsset != nullptr)
	//{
	//	mesh->PlayAnimation(AnimAsset, true);
	//}

}

// Called every frame
void AStudyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AStudyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	UE_LOG(tann, Log, TEXT("Setup Player Input Component"));
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AStudyPawn::LeftRight);
	PlayerInputComponent->BindAxis(TEXT("MoveFoward"), this, &AStudyPawn::UpDown);
}

void AStudyPawn::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(tann, Log, TEXT("pawn initialzed."));
}

void AStudyPawn::PossessedBy(AController* NewController) 
{
	UE_LOG(tann, Log, TEXT("pawn possessed."));
	Super::PossessedBy(NewController);
}

void AStudyPawn::UpDown(float scale)
{
	// UE_LOG(tann, Log, TEXT("up down : %f"), scale);
	AddMovementInput(GetActorForwardVector(), scale);
}

void AStudyPawn::LeftRight(float scale)
{
	// UE_LOG(tann, Log, TEXT("left right : %f"), scale);
	AddMovementInput(GetActorRightVector(), scale);
}

float AStudyPawn::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	UE_LOG(tann, Log, TEXT("pawn attacked. damage : %f"), damage);
	return damage;
}
