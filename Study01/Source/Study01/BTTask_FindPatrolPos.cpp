// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_FindPatrolPos.h"
#include "StudyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"

UBTTask_FindPatrolPos::UBTTask_FindPatrolPos()
{
	// NodeName = TEXT("FindPatrolPos");
}

EBTNodeResult::Type UBTTask_FindPatrolPos::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory)
{
	EBTNodeResult::Type result = Super::ExecuteTask(ownerComp, nodeMemory);
	AAIController* aiController = ownerComp.GetAIOwner();

	APawn* pawn =  aiController->GetPawn();
	if (pawn == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	UNavigationSystemV1* navSystem = UNavigationSystemV1::GetNavigationSystem(GetWorld());
	
	if (nullptr == navSystem) 
	{
		return EBTNodeResult::Failed;
	}

	FNavLocation nextLocation;
	bool navResult = navSystem->GetRandomPointInNavigableRadius(FVector::ZeroVector, 500.0f, nextLocation);
	if (navResult == false)
	{
		return EBTNodeResult::Failed;
	}

	ownerComp.GetBlackboardComponent()->SetValueAsVector(AStudyAIController::patrolPosKey, nextLocation.Location);
	return EBTNodeResult::Succeeded;
}