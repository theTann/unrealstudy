// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UMG.h"
#include "MainUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class STUDYUI_API UMainUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
	UFUNCTION()
	void OptionBtnClicked();

	UFUNCTION()
	void ChangeImageBtnClicked();

	UPROPERTY(EditAnywhere, Category = "PopupWidget")
	TSubclassOf<UUserWidget> optionWidget;

	UPROPERTY()
	UUserWidget* optionPopup;

	UPROPERTY()
	UProgressBar* hpProgressBar;

	UPROPERTY()
	UProgressBar* manaProgressBar;
};
