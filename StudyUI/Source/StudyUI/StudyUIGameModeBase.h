// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StudyUI.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "StudyUIGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STUDYUI_API AStudyUIGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	void OpenOptionWIdget();

	UPROPERTY(EditAnywhere, Category = "Widget")
	TSubclassOf<UUserWidget> mainWidget;

	UPROPERTY()
	UUserWidget* currentWidget;
};
